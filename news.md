# update

### 额外更新（2021-02-11）

不来呢是不准备更新GEOmirror了，因为GEOmirror功能都被我的annoprobe包给包含了，但是“一棵树”给我提了建议，修改了一个依赖包的形式。恰好很多粉丝也反馈说两年没有去备份新的GEO数据库的表达量芯片数据集了，我就在这个春节假期加班加点弄了一下，完成了一个较大版本的更新，并且发布在gitee上面，方便中国区的小伙伴们下载安装以及使用。

- batch-2019-11-17 （  from GSE1nnn to GSE119nnn ）
- batch-2021-02-02（  from GSE120nnn to GSE165nnn ）

### 2020-02-25 15:20:05

当初想的是最后一次更新，然后永久性抛弃它！

### 2019-12-05 12:06:04

Add a check function, if the GSE study is not in **our list**, the geoChina will stop. 

### 2019-11-30 08:59:17

For Windows user, the `download.file` ,Code written to download binary files must use `mode = "wb"`, but the problems incurred by a text transfer will only be seen on Windows.

### 2019-11-29 22:59:51

For Windows user, the function `tempfile()` show different performance, so I change it.


