# GEOmirror

#### 介绍
备份了GEO数据库里面的几乎全部的表达量芯片数据集的表达量矩阵和临床信息矩阵

#### 版本日志 
不来呢是不准备更新GEOmirror了，因为GEOmirror功能都被我的annoprobe包给包含了，但是“一棵树”给我提了建议，修改了一个依赖包的形式。恰好很多粉丝也反馈说两年没有去备份新的GEO数据库的表达量芯片数据集了，我就在这个春节假期加班加点弄了一下，完成了一个较大版本的更新，并且发布在gitee上面，方便中国区的小伙伴们下载安装以及使用。

- batch-2019-11-17 （  from GSE1nnn to GSE119nnn ）
- batch-2021-02-02（  from GSE120nnn to GSE165nnn ）

### 如何安装

理论上使用下面的代码就ok了，但是不同人的电脑配置不一样，有一些人电脑没有rstudio，仅仅是R语言本身。有一些人呢，电脑没有git环境，反正千奇百怪啦

```r
library(remotes)
url='https://gitee.com/jmzeng/GEOmirror.git' 
install_git(url)
```

最简单的办法就是下载这个https://gitee.com/jmzeng/GEOmirror/repository/archive/master.zip 压缩包，详见： https://gitee.com/jmzeng/GEOmirror

压缩包文件解压后，然后进入proj文件打开rstudio后，点击build

### 如何使用

对每个数据集来说，GEOmirror包都是提供一个 geoChina 函数帮助下载而已：

```
library(GEOquery)
library(GEOmirror)
## download GSE95166 data
# https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE95166
#eSet=getGEO('GSE95166', destdir=".", AnnotGPL = F, getGPL = F)[[1]]
library(GEOmirror)
eSet=geoChina('GSE95166')
eSet
eSet=eSet[[1]]
```

后续数据分析呢，是标准的表达量差异分析流程，我给出一个示例代码如下:

```r

probes_expr <- exprs(eSet);dim(probes_expr)
head(probes_expr[,1:4])
boxplot(probes_expr,las=2)

## pheno info
phenoDat <- pData(eSet)
head(phenoDat[,1:4])
# https://www.ncbi.nlm.nih.gov/pubmed/31430288

groupList=factor(c(rep('npc',4),rep('normal',4)))
table(groupList)
eSet@annotation
# GPL15314	Arraystar Human LncRNA microarray V2.0 (Agilent_033010 Probe Name version)

genes_expr=probes_expr
library("FactoMineR")
library("factoextra")
dat.pca <- PCA(t(genes_expr) , graph = FALSE)
dat.pca
fviz_pca_ind(dat.pca,
             geom.ind = "point",
             col.ind = groupList,
             addEllipses = TRUE,
             legend.title = "Groups"
)
library(limma)
design=model.matrix(~factor(groupList))
design
fit=lmFit(genes_expr,design)
fit=eBayes(fit)
DEG=topTable(fit,coef=2,n=Inf)
head(DEG)
# We observed that 2107 lncRNAs were upregulated
# while 2090 lncRNAs were downregulated by more than 2-fold,
# NKILA among these downregulated lncRNAs (Fig 1A, GSE95166).

## for volcano plot
df=DEG
attach(df)
df$v= -log10(P.Value)
df$g=ifelse(df$P.Value>0.05,'stable',
            ifelse( df$logFC >1,'up',
                    ifelse( df$logFC < -1,'down','stable') )
)
table(df$g)
df$name=rownames(df)
head(df)
library(ggpubr)
ggpubr::ggscatter(df, x = "logFC", y = "v", color = "g",size = 0.5,
                  label = "name", repel = T,
                  label.select =head(rownames(df)),
                  palette = c("#00AFBB", "#E7B800", "#FC4E07") )
detach(df)


x=DEG$logFC
names(x)=rownames(DEG)
cg=c(names(head(sort(x),100)),
     names(tail(sort(x),100)))
cg
library(pheatmap)
n=t(scale(t(genes_expr[cg,])))
n[n>2]=2
n[n< -2]= -2
n[1:4,1:4]
ac=data.frame(groupList=groupList)
rownames(ac)=colnames(n)
pheatmap(n,show_colnames =F,show_rownames = F,
         annotation_col=ac) 
```

其它数据集类似的下载方式：

```r
# GSE165216
library(GEOquery)
library(GEOmirror)
## download GSE165216 data
# https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE165216
#eSet=getGEO('GSE165216', destdir=".", AnnotGPL = F, getGPL = F)[[1]]
library(GEOmirror)
eSet=geoChina('GSE165216')
eSet
eSet=eSet[[1]]
```



